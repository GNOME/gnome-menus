# translation of mr.po to Marathi
# Marathi Translation of gnome-menus
# Copyright (C) 2005, 2006, 2008, 2009 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-menus package.
#
# FIRST AUTHOR: Gayatri Deshpande <gafinde@gmail.com>, 2005.
# Rahul Bhalerao <b.rahul.pm@gmail.com>, 2006.
# Sandeep Shedmake <sshedmak@redhat.com>, 2008, 2009.
# Sandeep Shedmake <sshedmak@redhat.com>, 2009, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: mr\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"menus&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-03-18 10:33+0000\n"
"PO-Revision-Date: 2013-03-21 18:45+0530\n"
"Last-Translator: Sandeep Shedmake <sshedmak@redhat.com>\n"
"Language-Team: Marathi <fedora-trans-mr@redhat.com>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"

#: ../desktop-directories/AudioVideo.directory.in.h:1
msgid "Sound & Video"
msgstr "आवाज व चलचित्र"

#: ../desktop-directories/AudioVideo.directory.in.h:2
msgid "Multimedia menu"
msgstr "मल्टीमिडीया मेन्यू"

#: ../desktop-directories/Development.directory.in.h:1
msgid "Programming"
msgstr "प्रोग्रामिंग"

#: ../desktop-directories/Development.directory.in.h:2
msgid "Tools for software development"
msgstr "सॉफ्टवेयर बनवण्यासाटीचे औजार"

#: ../desktop-directories/Education.directory.in.h:1
msgid "Education"
msgstr "शिक्षण"

#: ../desktop-directories/Game.directory.in.h:1
msgid "Games"
msgstr "खेळ"

#: ../desktop-directories/Game.directory.in.h:2
msgid "Games and amusements"
msgstr "खेळ व मनोरंजन"

#: ../desktop-directories/Graphics.directory.in.h:1
msgid "Graphics"
msgstr "चित्र-विज्ञान"

#: ../desktop-directories/Graphics.directory.in.h:2
msgid "Graphics applications"
msgstr "चित्र-विज्ञान उपकरणं"

#: ../desktop-directories/Network.directory.in.h:1
msgid "Internet"
msgstr "इंटरनेट"

#: ../desktop-directories/Network.directory.in.h:2
msgid "Programs for Internet access such as web and email"
msgstr "इंटरनेट वेब व ईमेल बघण्यासाठी प्रोग्राम"

#: ../desktop-directories/Office.directory.in.h:1
msgid "Office"
msgstr "कार्यालय"

#: ../desktop-directories/Office.directory.in.h:2
msgid "Office Applications"
msgstr "कार्यालय संबंधित उपकरणं"

#: ../desktop-directories/System-Tools.directory.in.h:1
msgid "System Tools"
msgstr "यंत्रणे संबंधित औजार"

#: ../desktop-directories/System-Tools.directory.in.h:2
msgid "System configuration and monitoring"
msgstr "यंत्रणेचे सुसुत्रीकरण व संचलन"

#: ../desktop-directories/Utility-Accessibility.directory.in.h:1
msgid "Universal Access"
msgstr "जागतीक प्रवेश"

#: ../desktop-directories/Utility-Accessibility.directory.in.h:2
msgid "Universal Access Settings"
msgstr "जागतीक प्रवेश संयोजना"

#: ../desktop-directories/Utility.directory.in.h:1
msgid "Accessories"
msgstr "शिवाय उपकरणं"

#: ../desktop-directories/Utility.directory.in.h:2
msgid "Desktop accessories"
msgstr "डेस्कटॉपशी निगडित शिवाय उपकरणं"

#: ../desktop-directories/X-GNOME-Menu-Applications.directory.in.h:1
msgid "Applications"
msgstr "उपकरणं"

#: ../desktop-directories/X-GNOME-Other.directory.in.h:1
msgid "Other"
msgstr "अन्य"

#: ../desktop-directories/X-GNOME-Other.directory.in.h:2
msgid "Applications that did not fit in other categories"
msgstr "उपकरणं जे दुसरया कुठल्याही गटांत बसले नाही"

#: ../desktop-directories/X-GNOME-Sundry.directory.in.h:1
msgid "Sundry"
msgstr "सनड्राय"

#: ../desktop-directories/X-GNOME-Utilities.directory.in.h:1
msgid "Utilities"
msgstr "युटिलिटिज"

#: ../desktop-directories/X-GNOME-Utilities.directory.in.h:2
msgid "Small but useful GNOME tools"
msgstr "छोटे परंतु उपयोगी GNOME साधने"

#: ../desktop-directories/X-GNOME-WebApplications.directory.in.h:1
msgid "Web Applications"
msgstr "वेब ॲप्लिकेशन्स्"

#: ../desktop-directories/X-GNOME-WebApplications.directory.in.h:2
msgid "Applications and sites saved from Web"
msgstr "वेबपासून साठवलेले ॲप्लिकेशन्स् व स्थळ"

#~ msgid "Hardware"
#~ msgstr "हार्डवेअर"

#~ msgid "Settings for several hardware devices"
#~ msgstr "विविध हार्डवेअर साधन करीता संयोजना"

#~ msgid "Personal"
#~ msgstr "व्यक्तिगत"

#~ msgid "Personal settings"
#~ msgstr "व्यक्तिगत संयोजना"

#~ msgid "System"
#~ msgstr "प्रणाली"

#~ msgid "System settings"
#~ msgstr "प्रणाली संयोजना"

#~ msgid "Menu Editor"
#~ msgstr "मेन्यू संपादक"

#~ msgid "Edit Menus"
#~ msgstr "मेन्यू संपादा"

#~ msgid "_Defaults"
#~ msgstr "पूर्वनिर्धारीत (_D)"

#~ msgid "_Menus:"
#~ msgstr "मेन्यूज् (_M):"

#~ msgid "_Applications:"
#~ msgstr "उपकरणं (_A):"

#~ msgid "Simple Menu Editor %s"
#~ msgstr "सोपे मेन्यू संपादक %s"

#~ msgid "Name"
#~ msgstr "शिर्षक"

#~ msgid "Show"
#~ msgstr "दाखवा"

#~ msgid ""
#~ "Cannot find home directory: not set in /etc/passwd and no value for $HOME "
#~ "in environment"
#~ msgstr ""
#~ "गृह निर्देशिका शोधता आली नाही: /etc/passwd निर्धारित नाही आणि $HOME या "
#~ "चलनासाठी पर्यावरणात मुल्य नाही"

#~ msgid "Internet and Network"
#~ msgstr "महाजाळ व संजाळ"

#~ msgid "Network-related settings"
#~ msgstr "संजाळ-संबंधी संयोजना"

#~ msgid "Look and Feel"
#~ msgstr "दृश्य व प्रभाव"

#~ msgid "Settings controlling the desktop appearance and behavior"
#~ msgstr "डेस्कटॉप दृश्य व वागणूक नियंत्रीत करणारी संयोजना"

#~ msgid "Administration"
#~ msgstr "व्यवथापन"

#~ msgid "Change system-wide settings (affects all users)"
#~ msgstr "प्रणाली-करीताचे संयोजना बदलवा (सर्व वापरकर्ते प्रभावीत होतात)"

#~ msgid "Personal preferences"
#~ msgstr "व्यक्तिगत प्राधान्यता"

#~ msgid "Preferences"
#~ msgstr "प्राधान्ये"

#~ msgid "Personal preferences and administration settings"
#~ msgstr "व्यक्तिगत प्राधान्यता व प्रशासकीय रचना"

#~ msgid "Menu file"
#~ msgstr "मेन्यू फाइल"

#~ msgid "MENU_FILE"
#~ msgstr "MENU_FILE"

#~ msgid "Monitor for menu changes"
#~ msgstr "मेन्यूमधील बदलांसाठी निरिक्षक"

#~ msgid "Include <Exclude>d entries"
#~ msgstr "<Exclude>केलेली प्रविष्टे समाविष्ट करा"

#~ msgid "Include NoDisplay=true entries"
#~ msgstr "NoDisplay=true प्रविष्टे समाविष्ट करा"

#~ msgid "Invalid desktop file ID"
#~ msgstr "अवैध कार्यस्थळ फाइल आयडी"

#~ msgid "[Invalid Filename]"
#~ msgstr "[अवैध फाइलनाम]"

#~ msgid " <excluded>"
#~ msgstr " <वगळले>"

#~ msgid ""
#~ "\n"
#~ "\n"
#~ "\n"
#~ "==== Menu changed, reloading ====\n"
#~ "\n"
#~ "\n"
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "\n"
#~ "==== मेन्यू बदलला, पुनःलोड होत आहे ====\n"
#~ "\n"
#~ "\n"

#~ msgid "Menu tree is empty"
#~ msgstr "मेन्यू वृक्ष रिकामे आहे"

#~ msgid "- test GNOME's implementation of the Desktop Menu Specification"
#~ msgstr "- GNOME चा कार्यस्थळ मेन्यू नोंदींचा अवलंब तपासा"
